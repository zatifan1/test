package com.zak;

import java.util.*;
import java.util.stream.*;

public class Main {

    public static void main(String[] args) {
        List<Integer> decimal = new ArrayList<>(
                Arrays.asList(1, -1, 0, 2, 2, 3, 4, 5, -7, 8, -11, -2, -4, 5, 6, -5, -2));
        Integer count = sumZero(decimal);
        Integer count1 = sumZero(decimal);
        System.out.println(count);
        System.out.println(count);
    }

    private static Integer sumZero(List<Integer> decimal) {
        Integer count = 0;
        final var sorted = decimal.stream().sorted().collect(Collectors.toList());
        for (int i = 0, j = sorted.size() - 1; i < j; i++) {
            while (sorted.get(i) + sorted.get(j) > 0) {
                j--;
            }
            if (sorted.get(i) + sorted.get(j) == 0) {
                count++;
                j--;
            }
        }
        return count;
    }


}
